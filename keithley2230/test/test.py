
'''
Created on August 2020
@author: Quan
a mini script to control keithley instrument
GUI is waiting for up-to-date
'''

import pyvisa
import time
import numpy as N
import matplotlib.pyplot as plt
import os,sys
import datetime #to decide how long we will process the measurement

class keithley():

    def __init__(self, instr_address):
        self.rm = pyvisa.ResourceManager()
        self.inst = self.rm.open_resource(instr_address)
        self.inst.timeout = 2000
        self.inst.boud_rate = 57600
        #self.inst.read_termination = '\n'
        #self.inst.write_termination = '\n'

    def get_info(self):
        """
        Returns the instrument manufacturer, model, serial number, and firmware
        revision level.
        """
        info = str(self.inst.query("*IDN?")).split(',')
        return (" Manufacturer: " + info[0] + '\n',
                "Model: " + info[1] + '\n',
                "Serial Number: " + info[2] + '\n',
                " Firmware Version: " + info[3])

    def get_model(self):
        """Returns model type ."""
        model = self.get_info()[1].split(":")[1].lstrip().split("\n")[0]
        if model != '2230-30-1': model = 0
        return str(model)
    def select_channel(self, NR1):
        """
        Selects channel number.
        Parameters
        ----------
        NR1 : int
            The channel number.
        """
        if int(NR1) in range(1, 4):
            self.inst.write(f"INST:NSEL {NR1}")
        else:
            raise ValueError("Value Error. Please enter 1, 2, or 3.")

    def set_channel(self, level):
        """
        Selects the channel.
        Parameters
        ----------
        level : str
            The channel.
        """
        if str(level).upper() in ["CH1", "CH2", "CH3"]:
            self.inst.write(f"INST {level.upper()}")
        elif int(level) in range(1, 4):
            self.inst.write(f"INST CH{level}")
        else:
            raise ValueError("Value Error. Please enter CH1, CH2, or CH3.")

    def get_channel(self):
        """Queries selected channel."""
        channel = str(self.inst.query("INST?"))
        return(channel)

    def meas_curr(self, level):
        """
        Initiates and executes a new current measurement or queries the
        new measured current on a specified channel or channels.
        Parameters
        ----------
        level : str, int
            The channel or channels on which to make a new current measurement.
        """
        if str(level).upper() in ["CH1", "CH2", "CH3", "ALL"]:
            curr_meas = str(self.inst.query(f"MEAS:CURR? {level.upper()}"))
            return(curr_meas)
        if int(level) in range(1, 4):
            curr_meas = str(self.inst.query(f"MEAS:CURR? CH{level}"))
            return(curr_meas)
        if int(level) == 123:
            curr_meas = str(self.inst.query(f"MEAS:CURR? ALL"))
            return(curr_meas)
        else:
            raise ValueError("Value Error. Please enter CH1, CH2, CH3, or "
                             "ALL.")

    def meas_power(self, level):
        """
        Initiates and executes a new power measurement or queries the new
        measured power.
        Parameters
        ----------
        level : str, int
            The channel or channels on which to make a new power measurement.
        """
        if str(level).upper() in ["CH1", "CH2", "CH3", "ALL"]:
            pow_meas = str(self.inst.query(f"MEAS:POW? {level.upper()}"))
            return (pow_meas)
        if int(level) in range(1, 4):
            pow_meas = str(self.inst.query(f"MEAS:POW? CH{level}"))
            return (pow_meas)
        if int(level) == 123:
            pow_meas = str(self.inst.query(f"MEAS:POW? ALL"))
            return (pow_meas)
        else:
            raise ValueError("Value Error. Please enter CH1, CH2, CH3, or "
                             "ALL.")

    def meas_volt(self, level):
        """
        Initiates and executes a new voltage measurement or queries the new
        measured voltage.
        Parameters
        ----------
        level : str, int
            The channel or channels on which to make a new voltage measurement.
        """
        if str(level).upper() in ["CH1", "CH2", "CH3", "ALL"]:
            #print(f"MEAS:VOLT? {level.upper()}")
            volt_meas = str(self.inst.query(f"MEAS:VOLT? {level.upper()}"))
            return (volt_meas)
        if int(level) in range(1, 4):
            volt_meas = str(self.inst.query(f"MEAS:VOLT? CH{level}"))
            return (volt_meas)
        if int(level) == 123:
            volt_meas = str(self.inst.query(f"MEAS:VOLT? ALL"))
            return (volt_meas)
        else:
            raise ValueError("Value Error. Please enter CH1, CH2, CH3, or "
                             "ALL.")

    def apply_volt(self, level, volt, curr):
        """
        Sets voltage and current levels on a specified channel with a single
        command message.
        Parameters
        ----------
        level : str, int
            The channel to apply the settings to.
        volt :  int
            The voltage value to apply.
        curr :  int
            The current value to apply.
        """
     
        if self.get_model() =='2230-30-1':
            print("access 2")
            if str(level.upper()) in ["CH1", "CH2"]:
                print("access CH1 2")
                if (str(volt).upper() and str(curr).upper() in
                        ["MAX", "MIN", "DEF", "UP", "DOWN"] or
                        0 <= float(volt) <= 30 and 0 <= float(curr) <= 1.5):
                    print("access 3")
                    self.inst.write(f"APPL {level.upper()}, {volt}, {curr}")
            elif str(level.upper()) == 'CH3':
                #print(f"access {volt.upper()}")
                if (str(volt).upper() and str(curr).upper() in
                        ["MAX", "MIN", "DEF", "UP", "DOWN"] or
                        0 <= float(volt) <= 6 and 0 <= float(curr) <=5 ):
                    self.inst.write(f"APPL {level.upper()}, {volt}, {curr}")
            else:
                raise ValueError("Value Error")
        elif self.get_model()==0:
            raise ValueError("can't find keithley 2230-30-1")
   
            
     
    def set_output(self, boolean):
        """
        Sets the output state of the presently selected channel.
        Parameters
        ----------
        boolean : bool
            The output state.
        """
        if str(boolean).upper() in ["ON", "OFF"]:
            self.inst.write(f"CHAN:OUTP {boolean.upper()}")
        elif int(float(boolean)) in range(2):
            self.inst.write(f"CHAN:OUTP {boolean}")
        else:
            raise ValueError("Value Error. Please enter ON, OFF, 0, or 1.")

    def get_output(self):
        """Returns the output state of the presently selected channel."""
        channel_output = str(self.inst.query("CHAN:OUTP?"))
        return (channel_output)

#initiate
instr_name='USB0::0x05E6::0x2230::9203573::INSTR'
myins=keithley(instr_name)

"""
#test
print(myins.get_info())
print(myins.get_model())
myins.set_channel(1)
myins.set_output('ON')
myins.apply_volt('CH1',2,1)
myins.apply_volt('CH2',15,1)
myins.apply_volt('CH3',6,1)
myins.set_channel(2)
myins.set_output('ON')
myins.set_channel(3)
myins.set_output('ON')

#OFF
myins.set_output('OFF')
myins.set_channel(2)
myins.set_output('OFF')
#set output for present selected channel
print(myins.meas_volt('CH1'))
print(myins.meas_volt('CH2'))
print(myins.meas_volt('CH3'))
"""
myins.set_channel(1)
myins.set_output('ON')
myins.apply_volt('CH1',2,1)

#config something here
t0=0.0
time_internal=0.02 #second
numpoints=100
measCH='CH1' #["CH1", "CH2", "CH3", "ALL"]
Voltage=[]
Current = []
t=[]



SaveFile=True
#DO the Measurement
plt.ion() #开启interactive mode 成功的关键函数
fig=plt.figure(0)

#decide how long we process the program
start_time=time.time()
process_time=2# unit:mintue
#ax=fig.add_subplot(1,1,1)
plt.grid(True) #添加网格

while int(time.time())<(start_time+process_time*60):
    for i in range(numpoints):
        V1=float(myins.meas_volt('CH1'))
        I1=float(myins.meas_curr('CH1'))
        Voltage.append(V1)
        Current.append(I1)
        t0=t0+1
        t.append(t0)
        plt.plot(t, Voltage,c='r',ls='-', marker='o', mec='b',mfc='w')
        #plt.show()
        #ax.lines.pop(1) 删除轨迹
        plt.pause(0.01)
        plt.clf()
        time.sleep(time_internal)
    print (len(Voltage))
    
    if SaveFile:
        with open('volt.txt','a') as volt:
            volt.writelines(Voltage)
            volt.write('\n')
        with open('curr.txt','a') as curr:
            curr.writelines(Current)
            curr.write('\n')
    Voltage=[]
    Current=[]
    t0=0
    t=[]
