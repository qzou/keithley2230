import pyvisa
'''
from Module import function/class as alias
'''
from keithleyConfig import keithley as K

myins=K('USB0::0x05E6::0x2230::9203573::INSTR')
#print(myins.get_info())

#print(myins.get_model())

myins.set_channel(1)
print(myins.get_channel())
myins.apply_volt(1,2,2)
#myins.set_output(1)
v=myins.meas_volt(1)
print(v)