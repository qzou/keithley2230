
"""
Created on date Augest 2020
@author: Quan
"""

import pyvisa
import time
import numpy as N
import matplotlib.pyplot as plt
import os,sys
from keithleyConfig import keithley as K

#initiate
instr_address=''
temp=pyvisa.ResourceManager()
temp_instr=temp.list_resources()
#find the first USB instr_address
for str in temp_instr:
    title=str.split('::')[0]
    if title=='USB0':instr_address=str
    break
if instr_address=='':
    print("can't find a USB plug-in instrument!")
    temp.close()
    sys.exit()
temp.close()

myinstr=K(instr_address)
#config something here
t0=0.0
time_internal=0.01 #second
numpoints=1000
measCH='CH1' #["CH1", "CH2", "CH3", "ALL"]
Voltage=[]
Current = []


SaveFile=False
#DO the Measurement
plt.ion() #开启interactive mode 成功的关键函数
fig=plt.figure()
ax=fig.add_subplot(1,1,1)
plt.grid(True) #添加网格

for i in range(numpoints):
    V1=K.meas_volt('CH1')
    I1=K.meas_curr('CH1')
    Voltage.append(V1)
    Current.append(I1)
    ax.scatter(V1,I1,c='b',marker='.') #散点图
    #ax.lines.pop(1) 删除轨迹
    plt.pause(0.001)
    plt.pause(time_internal)
    #time.sleep(time_internal)



if SaveFile:
    with open('volt.txt','a') as volt:
        volt.write(Voltage)
        volt.write('\n')
    with open('curr.txt','a') as curr:
        curr.write(Current)
        curr.write('\n')
